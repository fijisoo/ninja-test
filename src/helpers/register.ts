import { Product } from '../store/reducers/types';

export function checkQuantity (products: Product[]) {
    return products.reduce((previousValue, {quantity}) => {
        return previousValue + quantity;
    }, 0)
}

export function checkTotalSum (products: Product[]) {
    return products.reduce((previousValue, {quantity, price}) => {
        return previousValue + (quantity * price);
    }, 0)
}
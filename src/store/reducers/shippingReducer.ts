import { ShippingReducer } from './types';
import { CustomAction } from '../actions/actionsTypes';

const INITIAL_STORE = {
    name: '',
    adress: '',
    phone: 0,
    email: '',
    shippingOptions: {
        label: '',
        value: 0
    },
};

const shippingReducer = (store: ShippingReducer = INITIAL_STORE, action: CustomAction) => {
    switch(action.type){
        case 'SET_FORM_VALUE': {
            return Object.assign({}, store, {[action.payload.name]: action.payload.value});
        }
        case 'SET_SELCT_FORM_VALUE': {
            return Object.assign({}, store, {[action.payload.name]: action.payload.value});
        }
    }
    return store;
};

export default shippingReducer;
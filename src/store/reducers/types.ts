export interface Product {
    id: string;
    image: string;
    name: string;
    description: string;
    price: number;
    quantity: number;
}

export type PaymentMethods = 'pay7' | 'cash';

export interface CartReducer extends Error{
    products: Product[],
    isLoading: boolean,
}

export interface PaymentReducer {
    isRegulationChecked: boolean;
    paymentMethod: PaymentMethods;
}

export interface ShippingReducer {
    name: string;
    adress: string;
    phone: number;
    email: string;
    shippingOptions: {label: string, value: number};
}

export interface Error {
    errorMsg: string;
}
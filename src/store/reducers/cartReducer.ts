import { CartReducer } from './types';
import { CustomAction } from '../actions/actionsTypes';

const INITIAL_STORE = {
    products: [],
    isLoading: false,
    errorMsg: '',
};

const cartReducer = (state: CartReducer = INITIAL_STORE, action: CustomAction) => {
    switch (action.type) {
        case 'INCREMENT_PRODUCT': {
            const products2 = state.products;
            products2[action.payload].quantity++;
            return Object.assign({}, state, {products: products2});
        }
        case 'DECREMENT_PRODUCT': {
            const productsArr = state.products;
            productsArr[action.payload].quantity--;
            return Object.assign({}, state, {products: productsArr});
        }
        case 'STORE_PRODUCTS': {
            return Object.assign({}, state, {products: action.payload.data, isLoading: false});
        }
        case 'DELETE_PRODUCT': {
            const newProducts = state.products.filter((m, index) => {
                return index !== action.payload;
            });
            return Object.assign({}, state, {products: newProducts});
        }
        case 'SET_LOADER': {
            return Object.assign({}, state, {isLoading: true});
        }
        case 'SET_ERROR':{
            return Object.assign({}, state, {errorMsg: action.payload, isLoading: false});
        }
        default:
            return state;
    }
};

export default cartReducer;
import { PaymentMethods, PaymentReducer } from './types';
import { CustomAction } from '../actions/actionsTypes';

const INITIAL_STORE = {
    isRegulationChecked: false,
    paymentMethod: 'pay7' as PaymentMethods,
};

function paymentReducer (store: PaymentReducer = INITIAL_STORE, action: CustomAction){
    switch(action.type){
        case 'SET_REGULATIONS': {
            return Object.assign({}, store, {isRegulationChecked: !store.isRegulationChecked});
        }
        case 'SET_PAYMENT': {
            return Object.assign({}, store, {paymentMethod: action.payload as PaymentMethods})
        }
        default: {
            return store;
        }
    }
}

export default paymentReducer;
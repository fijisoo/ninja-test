import { combineReducers } from 'redux';

import cartReducer from './reducers/cartReducer';
import shippingReducer from './reducers/shippingReducer';
import paymentReducer from './reducers/paymentReducer';

export const rootReducer = combineReducers({
    cartReducer,
    shippingReducer,
    paymentReducer,
});
import { AxiosResponse } from 'axios';
import { actionsTypes } from './actionsTypes';

export function incrementProduct(id: number) {
    return {
        type: actionsTypes.INCREMENT_PRODUCT,
        payload: id,
    };
}

export function decrementProduct(id: number) {
    return {
        type: actionsTypes.DECREMENT_PRODUCT,
        payload: id,
    };
}

export function deleteProduct(id: number) {
    return {
        type: actionsTypes.DELETE_PRODUCT,
        payload: id,
    };
}

export function storeProducts(response: AxiosResponse) {
    return {
        type: actionsTypes.STORE_PRODUCTS,
        payload: response,
    };
}

export function setField(fieldData: { name: string, value: string | number }) {
    return {
        type: actionsTypes.SET_FORM_VALUE,
        payload: fieldData
    };
}

export function setSelect(selectData: {value: number, label: string}, selectName: string){
    return {
        type: actionsTypes.SET_SELCT_FORM_VALUE,
        payload: {name: selectName, value: selectData},
    }
}

export function setPayment(fieldData: string) {
    return {
        type: actionsTypes.SET_PAYMENT,
        payload: fieldData
    };
}

export function setRegulations() {
    return {
        type: actionsTypes.SET_REGULATIONS,
    };
}

export function setError(err: string) {
    return {
        type: actionsTypes.SET_ERROR,
        payload: err
    }
}

export function setLoader() {
    return {
        type: actionsTypes.SET_LOADER
    }
}
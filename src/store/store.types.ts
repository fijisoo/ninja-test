import { CartReducer, PaymentReducer, ShippingReducer } from './reducers/types';

export interface Store {
    cartReducer: CartReducer,
    shippingReducer: ShippingReducer,
    paymentReducer: PaymentReducer,
}
import { rootReducer } from './rootState';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger'
import thunk from 'redux-thunk';

export const configureStore = createStore(
    rootReducer,
    applyMiddleware(logger, thunk)
);
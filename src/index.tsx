import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import {configureStore} from './store/store';

import App from './App';

ReactDOM.render(
    <BrowserRouter>
        <Provider store={configureStore}>
            <App/>
        </Provider>
    </BrowserRouter>,
    document.getElementById('root') as HTMLElement
);

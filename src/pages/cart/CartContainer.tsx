import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CartReducer, Product } from '../../store/reducers/types';

import {
    incrementProduct,
    decrementProduct,
    deleteProduct,
    storeProducts,
    setError, setLoader
} from '../../store/actions';

import './_cart.scss';
import { ProductsServices } from '../../services/products';
import { CartProps, CartState } from './cart.types';
import { CartPage, SingleCart } from './CartPage';
import { Error } from '../Error';
import withSekeleton from './cart-item/withSkeleton';

class CartCreator extends Component<CartProps, CartState> {

    CartPageWithSkeleton = withSekeleton(CartPage);

    componentDidMount() {
        this.props.storeProducts();
    }

    generateItems = (): JSX.Element[] => {
        return this.props.cartReducer.products.map((item: Product, index: number) => {
                return SingleCart(item, index, this.props.addAnother, this.props.deleteOne, this.props.deleteProduct)
            }
        );
    };

    calculateSum = (): number => {
        return this.props.cartReducer.products.reduce((previousValue, {price, quantity}) => {
            return previousValue + (price * quantity);
        }, 0)
    };

    render() {
        return this.props.cartReducer.errorMsg ?
                <Error/> :
                <this.CartPageWithSkeleton
                    generateItems={this.generateItems}
                    calculateSum={this.calculateSum}
                    loading={this.props.cartReducer.isLoading}
                />;
    }
}

const mapStateToProps = ({cartReducer}: { cartReducer: CartReducer }) => {
    return {
        cartReducer,
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        addAnother(data: number) {
            dispatch(incrementProduct(data));
        },
        deleteOne(data: number) {
            dispatch(decrementProduct(data));
        },
        deleteProduct(data: number) {
            dispatch(deleteProduct(data));
        },
        storeProducts() {
            dispatch(setLoader());
            ProductsServices.fetchProducts().then(
                (response) => dispatch(storeProducts(response)),
                () => dispatch(setError('Something went wrong! Try again'))
            )
        },
    }
};

export const CartContainer = connect(mapStateToProps, mapDispatchToProps)(CartCreator);
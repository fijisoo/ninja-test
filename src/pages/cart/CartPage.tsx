import React from 'react';
import { Link } from 'react-router-dom';
import { CartItem } from './cart-item';

interface CartPageProps{
    generateItems: () => JSX.Element[];
    calculateSum: () => number;
}

export function SingleCart(singleProductData: any, index: number, addAnother: (productName: number) => void, deleteOne: (productName: number) => void, deleteProduct: (productName: number) => void) {
    const {id, image, name, description, price, quantity} = singleProductData;

    return (
        <CartItem
            id={index}
            image={image}
            title={name}
            description={description}
            price={price}
            quantity={quantity}
            incrementProduct={addAnother}
            decrementProduct={deleteOne}
            deleteProduct={deleteProduct}
            key={id}
        />
    );
}

export function CartPage (props: CartPageProps) {
  return(
      <div className="cart__group">
          {props.generateItems()}
          <div className="cart__group-sum">
              <p className="cart__group-sum-value">
                  {props.calculateSum()}€
              </p>
              <Link className="cart__group-sum-button" to={'/shipping'}>Pay</Link>
          </div>
      </div>
  )
}
import React, { Component } from 'react';

import * as BinImg from '../../../assets/images/delete.svg';

import './_cart-item.scss';

export interface ItemProps {
    id: number;
    image: string;
    title: string;
    description: string;
    price: number;
    quantity: number;
    isLoading?: boolean;
    incrementProduct: (productName: number) => void;
    decrementProduct: (productName: number) => void;
    deleteProduct: (productName: number) => void;
}

export class CartItem extends Component<ItemProps> {
    render() {
        return (
            <div className="cart-item">
                <div className="cart-item__image">
                    <img src={this.props.image} alt="cart item" className={'cart-item__image-item'}/>
                </div>
                <div className="cart-item__text">
                    <h3>{this.props.title}</h3>
                    <p>{this.props.description}</p>
                </div>
                <div className="cart-item__events">
                    <div className="cart-item__events-delete">
                        <span className="cart-item__events-delete-img" onClick={() => {this.props.deleteProduct(this.props.id)}}><img src={BinImg} alt="delete icon"/></span>
                    </div>
                    <div className="cart-item__events-price">
                        <div className="cart-item__events-price-quantite">
                            <button className="cart-item__events-price-quantite-button" disabled={this.props.quantity <= 1}
                                    onClick={() => this.props.decrementProduct(this.props.id)}>–
                            </button>
                            <p className="cart-item__events-price-quantite-value">{this.props.quantity}</p>
                            <button className="cart-item__events-price-quantite-button" disabled={this.props.quantity >= 100}
                                    onClick={() => this.props.incrementProduct(this.props.id)}>+
                            </button>
                        </div>
                        <div className="cart-item__events-price-costs">
                            <p>{(this.props.quantity * this.props.price).toFixed(2)}€</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
import React from 'react';
import './_cart-item.scss';

interface ComponentWithSkeletonProps {
    loading: boolean;
}

const withSekeleton = <T extends object>(Component: React.ReactType<T>) =>
    class WithSkeleton extends React.Component<T & ComponentWithSkeletonProps> {
        render() {
            return (
                this.props.loading ? (<div className="cart-item cart-item--loading">
                    <div className="cart-item__image">
                        <div className="cart-item__image-item cart-item__image-item--loading"/>
                    </div>
                    <div className="cart-item__text cart-item__text--loading">
                        <h3/>
                        <p/>
                        <p/>
                        <p/>
                        <p/>
                    </div>
                </div>) : <Component {...this.props}/>);
        }
    };

export default withSekeleton;
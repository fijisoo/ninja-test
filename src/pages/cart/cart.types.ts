import { CartReducer } from '../../store/reducers/types';

export interface CartProps {
    addAnother: (productName: number) => void;
    deleteOne: (productName: number) => void;
    deleteProduct: (productName: number) => void;
    storeProducts: () => void;
    cartReducer: CartReducer;
}

export interface CartState {
    isLoaded: boolean;
}
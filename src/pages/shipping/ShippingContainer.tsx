import React from 'react';
import { withFormik, FormikProps } from 'formik';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { setField, setSelect } from '../../store/actions';
import { checkQuantity, checkTotalSum } from '../../helpers/register';
import { ShippingPage } from './ShippingPage';
import { FormValues, InputValue, SelectOptions, SelectValue, ShippingProps } from './Shipping.types';
import './_shipping.scss';
import { ShippingReducer } from '../../store/reducers/types';
import { Omit } from 'react-router';

// TODO ANY ???
const ShippingWithFormk = withFormik<any, FormValues>({
    mapPropsToValues: props => {
        return props.shippingReducer;
    },
    validateOnBlur: true,
    validateOnChange: true,
    validationSchema: Yup.object().shape({
        name: Yup.string().required('This field is required').min(3, 'Must be at least 3 characters'),
        adress: Yup.string().required('This field is required'),
        phone: Yup.number().test('tel', 'Must be 9 digits', (test) => test >= 100000000 && test <= 999999999),
        email: Yup.string().email('Email is not valid').required('This field is required'),
        shippingOptions: Yup.object().test('shipping', 'Shipping didnt set', (data: SelectValue) => {
            return data.label !== '';
        }),
    }),
    handleSubmit: (values, {props}: any) => {
        props.history.push('/payment');
    },
})(ShippingForm);

const mapStateToProps = ({shippingReducer, cartReducer}: any) => {
    return {
        shippingReducer,
        cartReducer,
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        setField(data: { name: string | number, value: string }) {
            dispatch(setField({name: (data.name as string), value: data.value}));
        },
        setSelectField(data: {value: number, label: string}, selectName: string){
            dispatch(setSelect(data, selectName))
        }
    }
};

export function ShippingForm(props: ShippingProps & FormikProps<FormValues>) {
    const {touched, errors, isSubmitting, handleSubmit, setFieldValue, setFieldTouched, shippingReducer, cartReducer} = props;

    // TODO change prices in API to be eqivalent to selected shipping options
    const selectOptions: SelectOptions = [
        {value: 0, label: 'ninjPost', disable: true},
        {value: checkTotalSum(cartReducer.products) > 200 ? 0 : 15.99, label: 'D7L', disable: false},
        {value: checkTotalSum(cartReducer.products) > 200 ? 0 : 7.99, label: '7post', disable: false}
    ];

    const customStyles = {
        control: (base: any) => ({
            ...base,
            borderColor: touched.shippingOptions && errors.shippingOptions ? "#E2393E" : "hsl(0, 0%, 50%)"
        })
    };

    const setDisableSelectOptions = (option: { disable: boolean }) => {
        return option.disable && checkQuantity(cartReducer.products) <= 3;
    };

    function handleSelectChange(data: Omit<SelectValue,'disable'>, selectName: keyof ShippingReducer) {
        setFieldValue(selectName, data);
        setFieldTouched(selectName, true);
        const val: {value: number, label: string} = {value: data.value, label: data.label as string};
        props.setSelectField(val, selectName);
    }

    //TODO not working need typings improvement to handle select and input values
    function handleChange(data: InputValue) {
        const name: keyof ShippingReducer = (data as React.ChangeEvent<HTMLInputElement>).currentTarget.title as keyof ShippingReducer;
        const value = (data as React.ChangeEvent<HTMLInputElement>).currentTarget.value;
        setFieldValue(name, value);
        setFieldTouched(name, true);
        props.setField({name, value})
    }
    console.log('VALUESsSSSSSS', props.values);
    return <ShippingPage
        touched={touched}
        errors={errors}
        isSubmitting={isSubmitting}
        handleSubmit={handleSubmit}
        setFieldValue={setFieldValue}
        setFieldTouched={setFieldTouched}
        shippingReducer={shippingReducer}
        cartReducer={cartReducer}
        handleChange={handleChange}
        handleSelectChange={handleSelectChange}
        customStyles={customStyles}
        selectOptions={selectOptions}
        setDisableSelectOptions={setDisableSelectOptions}
    />;
}

export const ShippingContainer = connect(
    mapStateToProps, mapDispatchToProps
)(ShippingWithFormk);
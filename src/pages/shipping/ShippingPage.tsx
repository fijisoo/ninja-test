import React from 'react';
import Select from 'react-select';

interface ShippingPageProps{
    //TODO add used formik, selectStyles and handlers props
    [key: string]: any;
}

export function ShippingPage (props: ShippingPageProps) {
    const {touched, errors, isSubmitting, handleSubmit, shippingReducer, setDisableSelectOptions, handleChange, handleSelectChange, customStyles, selectOptions} = props;
    return(
      <div className="shipping">
          <form className="shipping__form" onSubmit={handleSubmit}>
              <label htmlFor="name" className="shipping__form-label">Name*
                  <div className="shipping__form-label-group">
                      <input value={shippingReducer.name} type="text" title="name" id="name"
                             className={`shipping__form-label-group-input ${touched.name && errors.name && 'shipping__form-label-group-input-invalid'}`}
                             onChange={handleChange}/>
                      {touched.name && errors.name &&
                      <span className="shipping__form-label-group-error">{errors.name}</span>}
                  </div>
              </label>
              <label htmlFor="adress" className="shipping__form-label">Address*
                  <div className="shipping__form-label-group">
                      <input type="text" title="adress" id="adress"
                             className={`shipping__form-label-group-input ${touched.adress && errors.adress && 'shipping__form-label-group-input-invalid'}`}
                             onChange={handleChange}/>
                      {touched.adress && errors.adress &&
                      <span className="shipping__form-label-group-error">{errors.adress}</span>}
                  </div>
              </label>
              <label htmlFor="phone" className="shipping__form-label">Phone
                  <div className="shipping__form-label-group">
                      <div className="shipping__form-label-group-icon">
                          <input type="number" title="phone" id="phone"
                                 className={`shipping__form-label-group-icon-input ${touched.phone && errors.phone && 'shipping__form-label-group-icon-input-invalid'}`}
                                 onChange={handleChange}/>
                      </div>
                      {touched.phone && errors.phone &&
                      <span className="shipping__form-label-group-error">{errors.phone}</span>}
                  </div>
              </label>
              <label htmlFor="email" className="shipping__form-label">Email
                  <div className="shipping__form-label-group">
                      <input type="text" title="email" id="email"
                             className={`shipping__form-label-group-input ${touched.email && errors.email && 'shipping__form-label-group-input-invalid'}`}
                             onChange={handleChange}/>
                      {touched.email && errors.email &&
                      <span className="shipping__form-label-group-error">{errors.email}</span>}
                  </div>
              </label>
              <label htmlFor="shipping-options" className="shipping__form-label">Shipping options
                  <div className="shipping__form-label-group">
                      <Select options={selectOptions}
                              name="shippingOptions"
                              id="shipping-options"
                              styles={customStyles}
                              isOptionDisabled={setDisableSelectOptions}
                              className="shipping__form-label-group-select"
                              onChange={(data)=>handleSelectChange(data, 'shippingOptions')}/>
                      {touched.shippingOptions && errors.shippingOptions &&
                      <span className="shipping__form-label-group-error">{errors.shippingOptions}</span>}
                  </div>
              </label>
              <button className="shipping__form-submit" type="submit" disabled={isSubmitting}>PAY</button>
          </form>
      </div>
  )
}
import { CartReducer, ShippingReducer } from '../../store/reducers/types';
import React from 'react';

export type ShippingOptions = 'ninjPost' | 'D7L' | '7post';

export interface SingleSelectOption {
    value: number;
    label: ShippingOptions,
    disable: boolean
}

export type SelectOptions = SingleSelectOption[];

export interface FormValues {
    name: string;
    adress: string;
    phone: number;
    email: string;
    shippingOptions: { name: string | number, value: number };
}

export interface ShippingProps {
    shippingReducer: ShippingReducer;
    cartReducer: CartReducer;
    setField: (data: { name: keyof ShippingReducer, value: string | number}) => void;
    setSelectField: (data: {value: number, label: string}, selectName: string) => void;
}


export type SelectValue = {value: any, label: string | number, disable: boolean};
export type InputValue = React.ChangeEvent<HTMLInputElement>
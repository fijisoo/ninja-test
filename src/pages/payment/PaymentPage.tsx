import React from 'react';
import { CartReducer, PaymentReducer, ShippingReducer } from '../../store/reducers/types';
import Select from 'react-select';
import * as _ from 'lodash';

import './_payment.scss';

interface PaymentProps {
    setRegulations: () => void;
    setPayment: (value: string) => void;
    paymentReducer: PaymentReducer;
    shippingReducer: ShippingReducer;
    cartReducer: CartReducer;
}

export function PaymentPageCreator(props: PaymentProps) {
    const {setRegulations, paymentReducer, shippingReducer, cartReducer, setPayment} = props;

    const paymentOptions = [
        {value: 'pay7', label: 'Pay7'},
        {value: 'cash', label: 'Cash payment'},
    ];

    const customStyles = {
        control: (base: any) => ({
            ...base,
            display: "flex",
        })
    };

    return (
        <div className="payment">
            <div className="payment-form">
                <div className="payment-form__section">
                    <h5>Summary</h5>
                    <p>
                        You will buy
                        <b className="bolded">{' ' + cartReducer.products.reduce((acumulator, reducer) => acumulator + reducer.quantity, 0) + ' '}</b>
                        products for

                        <b className="bolded">{' ' + cartReducer.products.reduce((acumulator, reducer) => acumulator + (reducer.quantity * reducer.price), 0) + '€ '}</b>
                        delivered by <b className="bolded">{' ' + shippingReducer.shippingOptions.label}</b>
                    </p>
                </div>
                <div className="payment-form__section">
                    <h5>Address</h5>
                    <p>{shippingReducer.name}</p>
                    <p>{shippingReducer.adress}</p>
                    <p>{shippingReducer.phone}</p>
                    <p>{shippingReducer.email}</p>
                </div>
                <div className="payment-form__section--horizontal">
                    <h5>Payment method</h5>
                    <Select options={paymentOptions}
                            styles={customStyles}
                            name="payment-section-select"
                            value={_.find(paymentOptions, {value: paymentReducer.paymentMethod})}
                            className="payment-form__section-select"
                            onChange={(e: any) => setPayment(e.value)}/>
                </div>
                <div className="payment-form__section">
                    <label className="payment-form__section-label">
                        <input type="checkbox" onClick={() => setRegulations()}/> I read and accept regulations
                    </label>
                </div>
            </div>
            <button className="payment-submit-button" disabled={!paymentReducer.isRegulationChecked}
                    onClick={() => console.log('pszeszlo')}>PAY
            </button>
        </div>
    )
}
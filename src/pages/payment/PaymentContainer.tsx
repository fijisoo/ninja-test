import React from 'react';
import { connect } from 'react-redux';
import { PaymentPageCreator } from './PaymentPage';
import { CartReducer, PaymentReducer, ShippingReducer } from '../../store/reducers/types';
import { Dispatch } from 'redux';
import { setPayment, setRegulations } from '../../store/actions';
import { Store } from '../../store/store.types';

interface PaymentContainerProps {
    setRegulations: () => void;
    setPayment: (value: string) => void;
    paymentReducer: PaymentReducer;
    shippingReducer: ShippingReducer;
    cartReducer: CartReducer;
}

export function PaymentContainerCreator(props: PaymentContainerProps) {
    const {setRegulations, paymentReducer, shippingReducer, cartReducer, setPayment} = props;
    return (
        <>
            <PaymentPageCreator
                setRegulations={setRegulations}
                paymentReducer={paymentReducer}
                shippingReducer={shippingReducer}
                cartReducer={cartReducer}
                setPayment={setPayment}
            />
        </>
    );
}

const mapStateToProps = ({paymentReducer, shippingReducer, cartReducer}: Store) => {
    return {paymentReducer, shippingReducer, cartReducer};
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        setRegulations() {
            dispatch(setRegulations());
        },
        setPayment(value: string) {
            dispatch(setPayment(value));
        }
    };
};

export const PaymentContainer = connect(mapStateToProps, mapDispatchToProps)(PaymentContainerCreator);


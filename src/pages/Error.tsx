import React from 'react';

interface ErrorProps{
  errorCode?: number
}

export function Error (props: ErrorProps) {
  return(
    <div>
      {props.errorCode}
      Something went wrong! Try again
    </div>
  )
}
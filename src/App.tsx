import * as React from 'react';
import { CartContainer } from './pages/cart';
import { ShippingContainer } from './pages/shipping';
import { Route } from 'react-router-dom';
import { Switch } from 'react-router';

import logo from './assets/images/logo.png';

import './_app.scss';
import { PaymentContainer } from './pages/payment/PaymentContainer';

class App extends React.Component {
    public render() {
        return (
            <div className="app">
                <div className="app__background"/>
                <div className="app__logo">
                    <img src={logo} alt="Logo" className="app__logo-img"/>
                    <p className="app__logo-text">7ninjas</p>
                </div>
                <div className="app__title">Front-End Developer</div>
                <div className="app__components">
                    <Switch>
                        <Route path={'/cart'} component={CartContainer}/>
                        <Route path={'/shipping'} component={ShippingContainer}/>
                        <Route path={'/payment'} component={PaymentContainer}/>
                    </Switch>
                </div>
            </div>
        );
    }
}

export default App;

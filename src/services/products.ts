import axios from 'axios';

export namespace ProductsServices {
    export function fetchProducts () {
        return axios.get('https://5b89d1686b7dcb0014d5f430.mockapi.io/api/products/');
    }
}